

def tick args 
  args.state.game_controller ||= Game::Controller.new(args).set_as_current

  Drake::Screen.current.tick args
end
