
module Game 

  class Player < Entity
    MOVEMENT_SPEED = 200/60 # px/frame

    def initialize ent 
      super ent

      @sprite = { x: @x, y: @y, w: @width, h: @height, 
                  path: "game/actors/player/sprites/player-idle.png",
                  flip_horizontally: false }
    end


    def update args 
      dx = args.inputs.left_right 
      dy = args.inputs.up_down 

      if dx.abs + dy.abs >= 2
        il = 1.0 / Math.sqrt( dx*dx + dy*dy )
        dx *= il 
        dy *= il
      end

      if dx > 0
        @sprite.flip_horizontally = false 
      elsif dx < 0
        @sprite.flip_horizontally = true 
      end

      @x += dx * MOVEMENT_SPEED
      @y += dy * MOVEMENT_SPEED
    end

    def render args 
      @sprite.x = @x 
      @sprite.y = @y 
      args.outputs[:scene].sprites << @sprite
    end

  end

end
