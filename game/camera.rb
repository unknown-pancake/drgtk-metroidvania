
module Game 
  class Camera 
    attr_accessor :cx, :cy, :width, :height, :zoom


    def initialize 
      @cx = 0
      @cy = 0
      @width = 1280
      @height = 720
      @zoom = 2.0
    end



    def rect_inside? rect 
      $gtk.args.geometry.inside_rect? self, rect
    end

    def position_inside? x0, y0
      x0 >= x && x0 <= x+w && y0 >= y && y0 <= y+h
    end

    def point_inside? pt
      poisition_inside? pt.x, pt.y
    end

    def rect_center_inside? rect 
      position_inside?( rect.x+rect.w*0.5, rect.y+rect.h*0.5 )
    end

    def intersect_rect? rect 
      $gtk.args.geometry.intersect_rect? self, rect
    end



    def x 
      @cx - final_width*0.5
    end

    def x= v 
      @cx = v + final_width*0.5
    end

    def y
      @cy - final_height*0.5
    end

    def y= v 
      @cy = v + final_height*0.5
    end

    def final_width 
      @width / @zoom 
    end

    def final_width= v 
      @width = v * @zoom
    end

    def final_height 
      @height / @zoom 
    end

    def final_height= v 
      @height = v * @zoom
    end

    alias w final_width 
    alias h final_height 
  end
end
