
module Game 
  SCALE = 2

  class Controller < Drake::Screen 
    attr_reader :camera, :world

    def start args 
      @entity_registry = EntityRegistry.new
      @camera = Camera.new
      @player = nil
      @world = World.new self, @entity_registry, @camera

      add_background_fiber @world.load("game/world/metroidvania.ldtk", "Level_0", args)
    end

    def render args 
      prepare_scene args
      
      @world.render args

      render_scene args
    end

    def update args 
      #@camera.cx, @camera.cy = @player.x, @player.y
      @camera.cx += args.inputs.left_right * 200/60
      @camera.cy += args.inputs.up_down * 200/60

      @world.update args
    end



    def prepare_scene args 
      args.outputs[:scene].w = @camera.final_width
      args.outputs[:scene].h = @camera.final_height
    end

    def render_scene args 
      args.outputs[:scene].sprites.each do |spr| 
        spr.x = (spr.x - @camera.x).floor
        spr.y = (spr.y - @camera.y).floor
      end

      args.outputs.sprites << { x: 0, y: 0, 
                                w: @camera.width, h: @camera.height,
                                path: :scene }

      args.outputs.primitives << { x: 0, y: 0, w: 256, h: 256, r: 255, g: 255, b: 255 }.solid!
      args.outputs.primitives << { x: 300, y: 0, w: 256, h: 256, r: 255, g: 255, b: 255 }.solid!
      args.outputs.primitives << { x: 0, y: 0, w: 256, h: 256, r: 255, g: 0, b: 255 }.border!
      args.outputs.primitives << { x: 300, y: 0, w: 256, h: 256, r: 255, g: 0, b: 255 }.border!
      args.outputs.primitives << { x: 0, y: 0, w: 256, h: 256, path: :world_chunk_rendering_buffer0 }.sprite!
      args.outputs.primitives << { x: 300, y: 0, w: 256, h: 256, path: :world_chunk_rendering_buffer1 }.sprite!
      args.outputs.sprites << { x: 0, y: 300, w: 256, h: 256, path: :Level_0__Background }
      args.outputs.sprites << { x: 300, y: 300, w: 256, h: 256, path: :Level_0__Foreground }
    end

  end

end


