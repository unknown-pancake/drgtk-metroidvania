
module Game 

  class Entity 
    attr_accessor :x, :y, :width, :height

    def initialize ent_inst 
      @x, @y = ent_inst.px_x, ent_inst.px_y
      @width, @height = ent_inst.width, ent_inst.height

      @parameters = ent_inst.fields.map do |fi| 
        [fi.identifier.to_sym , fi.value]
      end.to_h
    end


    def param name 
      @parameters[name]
    end

    def set_param name, value 
      @parameters[name] = value 
      value
    end


    def update args 
    end

    def render args 
    end


    def self.attr_params(*syms)
      syms.each do |sym| 
        define_method sym {
          param sym
        }
    
        setter = (sym.id2name + "=").to_sym 
        define_method setter { |v|
          set_param setter v
        }
      end
    end
  end

end
