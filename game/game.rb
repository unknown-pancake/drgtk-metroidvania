
module Game
  extend self
end


require 'game/entity.rb'
require 'game/camera.rb'
require 'game/controller.rb'
require 'game/world.rb'

require 'game/actors/player/player.rb'
