
module Game 

  class World 
    attr_reader :chunks

    def initialize screen, reg, cam
      @screen = screen
      @entity_registry = reg
      @camera = cam

      @ldtk = nil
      @chunks = {}
      @active_chunk = nil

      @loaded = false 
      @fiber = nil
    end


    def render args 
      @chunks.each { |_, chk| chk.render args }
    end

    def update args 
      return if @active_chunk == nil

      if _camera_center_out_of_active_chunk_bound? 
        _mark_all_chunk_for_deletion
        _disable_active_chunk
        _find_new_active_chunk
        _check_active_chunk_found
        _keep_and_activate_new_active_chunk_loaded
        _keep_loaded_or_load_active_chunk_neighbours args
        _unload_and_delete_chunks_marked_for_deletion args
      end

      _update_all_chunks args
    end

    def _camera_center_out_of_active_chunk_bound? 
      !@active_chunk.position_inside? @camera.cx, @camera.cy
    end

    def _mark_all_chunk_for_deletion
      @chunks.each { |_, chk| chk.queue_deletion = true }
    end

    def _disable_active_chunk
      @active_chunk.active = false
      @active_chunk = nil
    end
    
    def _find_new_active_chunk
      @chunks.each do |_, chk|
        if chk.position_inside? @camera.cx, @camera.cy 
          @active_chunk = chk
          break
        end
      end
    end

    def _check_active_chunk_found
      if @active_chunk == nil 
        raise "Unable to find the new active chunk?!"
      end
    end

    def _keep_and_activate_new_active_chunk_loaded
      @active_chunk.queue_deletion = false
      @active_chunk.active = true
    end

    def _keep_loaded_or_load_active_chunk_neighbours args 
      @active_chunk.level.neighbours.each do |n| 
        if @chunks.has_key? n.level_uid 
          @chunks[n.level_uid].queue_deletion = false
        else
          _load_chunk n.level_uid, args
        end
      end
    end

    def _unload_and_delete_chunks_marked_for_deletion args
      @chunks.delete_if do |_, chk| 
        chk.unload args if chk.queue_deletion
        chk.queue_deletion
      end
    end

    def _update_all_chunks args 
      @chunks.each { |_, chk| chk.update args }
    end



    def load path, initlevel, args
      return nil if @loaded 

      @fiber = Fiber.new {
        @ldtk = LDTK::Project.new path 

        Fiber.yield 

        lvl = @ldtk.fetch_level_with_identifier initlevel, true
        
        Fiber.yield

        chk = _new_chunk lvl
        chk_fiber = chk.load args

        while chk_fiber.alive? 
          chk_fiber.resume
          Fiber.yield
        end

        chk.active = true 
        chk.visible = true 
        @active_chunk = chk
        @chunks[lvl.uid] = chk
        
        lvl.neighbours.each do |neighbour| 
          _load_chunk neighbour.level_uid, args
        end

        @loaded = true
      }
      @fiber
    end

    def _load_chunk uid, args
      lvl = @ldtk.fetch_level_with_uid uid, true
      chk = _new_chunk lvl

      @chunks[lvl.uid] = chk

      @screen.add_background_fiber chk.load(args), :world_chunk_loading, 190, 3, 252
    end

    def _new_chunk lvl 
      WorldChunk.new lvl, @entity_registry, @ldtk.cell_width, @ldtk.cell_height
    end

  end

  class WorldChunk 
    attr_accessor :active, :visible, :queue_deletion, :render_targets
    attr_reader :level, :loaded, 
                :cell_x, :cell_y, :pixel_x, :pixel_y, 
                :cell_w, :cell_h, :pixel_w, :pixel_h


    def initialize lvl, reg, cw, ch
      @level = lvl
      @entity_registry = reg

      @cell_x, @cell_y = (lvl.world_x / cw).floor, (lvl.world_y / ch).floor
      @pixel_x, @pixel_y = lvl.world_x, lvl.world_y
      @cell_w, @cell_h = (lvl.width / cw).floor, (lvl.height / ch).floor
      @pixel_w , @pixel_h = lvl.width, lvl.height

      @collision_layer = nil
      @tile_layers = []
      @entities = []
      @render_targets = []

      @active = false
      @visible = false
      @queue_deletion = false

      @loaded = false
      @fiber = nil
    end



    def update args 
      return unless @active 

      @entities.each { |e| e.update args }
    end

    def render args 
      #return unless @visible

      if @loaded 
        @render_targets.each do |rt|
          args.outputs[:scene].sprites << { x: @pixel_x, y: @pixel_y, 
                                            w: @pixel_w, h: @pixel_h, 
                                            path: rt }
        end
      else
        args.outputs[:scene].borders << { x: @pixel_x, y: @pixel_y, 
                                          w: @pixel_w, h: @pixel_h, 
                                          r: 255, g: 0, b: 255 }
      end

      @entities.each { |e| e.render args }
    end


    def position_inside? x, y 
      x >= @pixel_x && x <= @pixel_x + @pixel_w &&
      y >= @pixel_y && y <= @pixel_y + @pixel_h
    end


    def unload args
      puts "[#{@level.identifier}] Unloading..."
      @render_targets.each do |rt|
        args.outputs[rt].w = 1
        args.outputs[rt].h = 1
      end
    end

    def load args
      return nil if @loaded

      @fiber = Fiber.new {
        puts "[#{@level.identifier}] Loading data..."
        @level.fetch

        Fiber.yield # level.fetch could be pretty long to do

        _fetch_layers

        buffers = [
          :world_chunk_rendering_buffer0,
          :world_chunk_rendering_buffer1
        ]
        
        last_swap_tick = args.tick_count

        cbuf = -> { buffers[0] }
        swpb = -> { 
          # the buffer doesn't need to be swapped if the fiber is resumed 
          # in the same frame
          return if last_swap_tick == args.tick_count 
          last_swap_tick = args.tick_count

          b0 = buffers[0]
          b1 = buffers[1]
          buffers.reverse! 

          args.outputs[b1].w = @level.width 
          args.outputs[b1].h = @level.height
          args.outputs[b1].sprites << { x: 0, y: 0, 
                                        w: @level.width, h: @level.height,
                                        path: b0 } 
        }
        
        @tile_layers.each do |l|
          puts "[#{@level.identifier}] Prerendering layer #{l.identifier}"
          buffers.each do |b| 
            args.outputs[b].w = @level.width 
            args.outputs[b].h = @level.height 
          end

          tiles = nil
          case l.type 
          when :tiles
            tiles = l.grid_tiles.size
          when :autolayer 
            tiles = l.autolayer_tiles
          end
          
          tiles.each_slice(50) do |ts| 
            Fiber.yield 
            swpb[]
            
            args.outputs[cbuf[]].sprites << ts
          end

          rt = "#{@level.identifier}__#{l.identifier}".to_sym
          args.outputs[rt].w = @level.width
          args.outputs[rt].h = @level.height
          args.outputs[rt].sprites << { x: 0, y: 0, 
                                        w: @level.width, h: @level.height,
                                        path: cbuf[] }

          @render_targets << rt

          # we need to wait for the next frame in order to reset the buffers for
          # the next layer
          Fiber.yield true 
        end

        buffers.each do |b| 
          args.outputs[b].w = 1
          args.outputs[b].h = 1
        end

        puts "[#{@level.identifier}] Loading complete."

        @loaded = true
      }
      @fiber
    end


    def _fetch_layers 
      entity_layer = nil

      @level.layer_instances.each do |l| 
        if l.identifier == "collisions" 
          @collision_layer = l
        else
          case l.type 
          when :entities 
            entity_layer = l
          when :tiles, :autolayer
            @tile_layers << l
          end
        end
      end

      entity_layer.entity_instances.each do |ent| 
        e = @entity_registry.construct ent
        if e
          e.x += @pixel_x
          e.y += @pixel_y
          @entities << e
        end
      end
    end

  end



  class EntityRegistry 

    def initialize 
      @factories = {}
    end


    def register name, &blk 
      if @factories.include? name 
        raise "A factory is already registered for the entity '#{name}'"
      end
      @factories[name] = blk
      self
    end


    def construct ent
      if @factories.include? ent.identifier 
        @factories[name].call ent
      else
        puts "No factory defined for '#{ent.identifier}', ignoring."
        nil
      end
    end

  end

end
