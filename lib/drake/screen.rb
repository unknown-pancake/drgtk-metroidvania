
module Drake 

  class Screen

    def initialize args
      @time_tracker = TimeTracker.new
      @background_fibers = []

      dur = @time_tracker.measure :screen_start, 255, 0, 0 do 
        start args
      end

      puts "* ~#{self.class.name}~ | The screen took #{(dur*1_000_000).floor}µs to start."
    end

    def tick args
      @time_tracker.measure :screen_render, 0, 255, 0 do 
        render args 
      end

      @time_tracker.measure :screen_update, 0, 255, 255 do 
        update args
      end

      run_background_fibers

      @time_tracker.tick args
    end


    def set_as_current
      Screen::current = self
      self
    end


    def add_background_fiber(fiber, name=:bg_fiber, r=255, g=0, b=255)
      @background_fibers << { fiber: fiber, name: name, r: r, g: g, b: b }
    end


    def start args 
    end

    def render args 
    end

    def update args 
    end


    def run_background_fibers
      while @time_tracker.time_left > 0.004_000 && !@background_fibers.empty?
        entry = @background_fibers[0]

        if entry.fiber.alive?
          wait_for_frame = false
          @time_tracker.measure entry.name, entry.r, entry.g, entry.b do 
            wait_for_frame = entry.fiber.resume
          end
          break if wait_for_frame
        else
          puts "*** TimeTracker | Fiber #{entry.name} finished."
          @background_fibers.shift
        end
      end
    end


    def self.current
      @current
    end

    def self.current= v
      @current = v 
    end

  end

end
