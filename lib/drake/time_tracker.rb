
module Drake 

  class TimeTracker
    attr_accessor :visible
    attr_reader :time_left

    FRAME_DURATION = 0.016_667 # s
    MAX_MARGIN = 0.004_000 # s
    MAX_DURATION_USED = FRAME_DURATION - MAX_MARGIN
    HEIGHT = 16
    

    def initialize
      @visible = true
      @durations = []
      @time_left = MAX_DURATION_USED
      @epoch = Time.new.to_f
    end



    def tick args 
      if args.inputs.keyboard.key_down.one
        @visible = !@visible 
      end

      if @visible
        render args
      end

      if @time_left < -MAX_MARGIN then
        $gtk.notify! "The frame took more than 16ms!"
        puts "* WARN: The frame took more than 16ms!"
      end

      @durations = []
      @time_left = MAX_DURATION_USED
    end

    def render args 
      primitives = []

      primitives << { x: 0, y: 0, w: 1280, h: HEIGHT, r: 0, g: 0, b: 0 }.solid!

      if @time_left < 0 then
        primitives << { x: 0, y: 0, w: 1280, h: HEIGHT, r: 255, g: 0, b: 0 }.border!
      else
        primitives << { x: 0, y: 0, w: 1280, h: HEIGHT, r: 255, g: 255, b: 255 }.border!
      end

      x = 2
      @durations.each do |dur| 
        w = (1280-2*2) * (dur.duration / MAX_DURATION_USED)
        primitives << { x: x, y: 2, w: w, h: HEIGHT - 4, 
                        r: dur.r, g: dur.g, b: dur.b }.solid!
        
        x += w+1
      end

      args.outputs.debug << primitives
      args.outputs.debug << args.gtk.framerate_diagnostics_primitives
    end


    def start name, r, g, b
      return if !@durations.empty? and @durations.last.name == name

      @durations << Entry.new(name, now, r, g, b)
    end

    def finish name
      raise "No measure started" if @durations.empty? == 0
      entry = @durations.last
      entry.finish = now

      @time_left -= entry.duration

      entry.duration
    end

    def measure name, r, g, b, &blk
      start name, r, g, b
      res = blk.call
      finish name
    end


    def now
      Time.new.to_f - @epoch
    end


    class Entry 
      attr_accessor :name, :r, :g, :b, :start, :finish

      def initialize name, start, r, g, b
        @name = name
        @r, @g, @b = r, g, b
        @start = start
        @finish = 0
      end

      def duration 
        @finish - @start
      end
    end

  end

end