
module LDTK 

  class LevelNeighbour
    attr_reader :direction, :level_uid 


    def initialize data 
      @direction = case data["dir"]
        when "n" then :north 
        when "s" then :south
        when "w" then :west 
        when "e" then :east 
        else raise "Unknown neighbour direction '#{data["dir"]}'"
      end

      @level_uid = data["levelUid"]
    end
  end

  class FieldInstance 
    attr_reader :identifier, :type, :value


    def initialize data 
      @identifier = data["__identifier"]
      @raw_value = data["__value"]
      @type = data["__type"]

      case @type 
      when "Int", "Float", "Bool", "String", "FilePath", "Color"
        @value = @raw_value 
      when "Point"
        @value = [@raw_value["cx"], @raw_value["cy"]]
      else
        puts "Unrecognized field type '#{@type}'. Setting value to nil."
        @value = nil
      end
    end


    def is_array?
      @raw_value.is_a? Array
    end
  end

  class EntityInstance
    attr_reader :grid_x, :grid_y, :identifier, :pivot_x, :pivot_y, :fields, 
                :width, :height, :px_x, :px_y


    def initialize data 
      @grid_x, @grid_y = data["__grid"]
      @identifier = data["__identifier"]
      @pivot_x, @pivot_y = data["__pivot"]
      @fields = data["fieldInstances"].map { |d| FieldInstance.new d }
      @width, @height = data["width"], data["height"]
      @px_x, @px_y = data["px"]
    end
  end

  class LayerInstance 
    attr_reader :grid_height, :grid_width, :grid_size, :identifier, :opacity,
                :offset_x, :offset_y, :tileset_path, :type, 
                :autolayer_tiles, :entity_instances, :grid_tiles, :int_grid, 
                :level_uid, :visible 


    def initialize data, base_dir 
      @identifier = data["__identifier"]
      @level_uid = data["levelId"]
      @type = Project::convert_layer_type_string data["__type"]
      
      @opacity = data["__opacity"]
      @visible = data["visible"]

      @tileset_path = data["__tilesetRelPath"] && base_dir + data["__tilesetRelPath"]
      @grid_height, @grid_width = data["__cHei"], data["__cWid"]
      @offset_x, @offset_y = data["__pxTotalOffsetX"], data["__pxTotalOffsetY"]
      @grid_size = data["__gridSize"]

      @autolayer_tiles = data["autoLayerTiles"] && data["autoLayerTiles"].map { |t| _make_tile t }
      @entity_instances = data["entityInstances"].map do |e| 
        e["px"][1] = (@grid_height - 1) * @grid_size - e["px"][1]
        EntityInstance.new e 
      end
      @grid_tiles = data["gridTiles"].map { |t| _make_tile t }
      @int_cells = data["intGridCsv"]
    end


    def _make_tile data 
      {
        x: @offset_x + data["px"][0],
        y: -@offset_y + (@grid_height - 1) * @grid_size - data["px"][1],
        w: @grid_size,
        h: @grid_size,
        path: @tileset_path,
        tile_x: data["src"][0],
        tile_y: data["src"][1],
        tile_w: @grid_size, 
        tile_h: @grid_size,
        flip_horizontally: (data["f"] & 1) != 0,
        flip_vertically: (data["f"] & 2) != 0
      }
    end
  end


  class Level 
    attr_reader :background_color, :neighbours, :relative_path, 
                :fields, :identifier, :layers, :layer_instances, :width, :height, 
                :uid, :world_x, :world_y


    def initialize data, base_dir
      @base_directory = base_dir
      @background_color = data["__bgColor"]
      @neighbours = data["__neighbours"].map { |d| LevelNeighbour.new d }
      @relative_path = data["externalRelPath"]
      @field = data["fieldInstances"].map { |d| FieldInstance.new d }
      @identifier = data["identifier"] 
      @layer_instances = _process_layer_instances data["layerInstances"]
      @width, @height = data["pxWid"], data["pxHei"]
      @uid = data["uid"] 
      @world_x, @world_y = data["worldX"], -data["worldY"] - @height

      @layers = @layer_instances && @layer_instances.map { |l| [l.identifier, l] }.to_h
    end


    def is_partial?
      @layer_instances == nil
    end


    def fetch 
      unless is_partial?
        return
      end

      path = @base_directory + @relative_path
      data = $gtk.parse_json_file(path)

      @layer_instances = _process_layer_instances data["layerInstances"]
    end


    def _process_layer_instances dat
      return nil unless dat

      dat.map { |d| LayerInstance.new d, @base_directory }.reverse!
    end

  end

end