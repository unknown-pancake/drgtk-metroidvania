
module LDTK 
  extend self


  class Project
    attr_reader :base_directory, :background_color, 
                :cell_width, :cell_height, :world_layout


    def initialize path 
      _compute_base_directory path
      _fetch_data $gtk.parse_json_file(path)
    end


    def is_monolithic_project?
      !@_external_levels
    end

    def is_free_world?
      @world_layout == :free
    end

    def is_gridvania_world?
      @world_layout == :gridvania 
    end

    def is_linear_horizontal_world?
      @world_layout == :linear_horizontal
    end

    def is_linear_vertical_world?
      @world_layout == :linear_vertical
    end

    def has_level_identifier? name 
      level_identifiers.include? name 
    end

    def has_level_uid? uid 
      level_uids.include? uid
    end


    def cell_size 
      [@cell_width, @cell_height]
    end

    def level_identifiers
      @_level_identifiers ||= @_levels.map { |_, dat| dat["identifier"] }
      @_level_identifiers
    end

    def level_uids
      @_level_uids ||= @_levels.keys 
      @_level_uids
    end



    def fetch_level_with_identifier name, partialok=false 
      raise "Level '#{name}' not found." unless has_level_identifier? name

      lvl = @_levels.each_value { |lvl| break lvl if lvl["identifier"] == name }
      _fetch_level lvl, partialok
    end

    def fetch_level_with_uid uid, partialok=false 
      raise "Level '#{uid}' not found." unless has_level_uid? uid 

      lvl = @_levels[uid]
      _fetch_level lvl, partialok
    end


    def _compute_base_directory path 
      @base_directory = path.split("/")[0..-2].join("/") + "/"
    end

    def _fetch_data data 
      @background_color = data["bgColor"]
      @cell_width = data["worldGridWidth"]
      @cell_height = data["worldGridHeight"]
      @world_layout = Project::convert_world_layout_string data["worldLayout"]
      
      @_external_levels = data["externalLevels"]
      @_levels = data["levels"].map { |l| [ l["uid"], l ] }.to_h
    end

    def _fetch_level lvl, partialok 
      if is_monolithic_project? || partialok
        Level.new lvl, @base_directory
      else
        _load_external_level lvl
      end
    end

    def _load_external_level lvl 
      path = @base_directory + lvl["externalRelPath"]
      Level.new $gtk.parse_json_file(path), @base_directory
    end



    def self.convert_world_layout_string layout 
      case layout
      when "Free"             then :free 
      when "GridVania"        then :gridvania 
      when "LinearHorizontal" then :linear_horizontal
      when "LinearVertical"   then :linear_vertical
      else raise "Invalid world layout '#{data["worldLayout"]}'"
      end
    end

    def self.convert_layer_type_string type 
      case type
      when "IntGrid"   then :int_grid
      when "Entities"  then :entities 
      when "Tiles"     then :tiles 
      when "AutoLayer" then :autolayer 
      else raise "Unknown layer type '#{type}'"
      end
    end

  end

end
